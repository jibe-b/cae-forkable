# Qu'est-ce qu'un fork ?

# Qu'est-ce que _git_ et les logiciels de gestion de versions ?

# Que se passe-t-il si je forke un projet ?

# Ai-je toujours le droit de forker un projet ?

# Peut-on revenir en arrière après un fork ?

# Est-il possible de fusionner deux projets ?

# Peut-on m'imposer des conditions si je forke un projet ?

# Comment gère-t-on les histoires d'argent quand un fork a lieu ?