# CAE liquide




## Regroupement d'individus en consensus sur la validité d'une charte

La participation à une CAE implique l'acceptation des statuts et l'adhésion à la charte intérieure adjointe aux statuts.

Au niveau du groupe, un consensus est à maintenir. L'évolution des attentes des individus peut ainsi mener à des mises à jour de cette charte ou à un fork.



_Pour cette raison, la charte est fondamentale. Sa construction gagne à impliquer chacun·e des membres de la CAE, et à sa mise à jour pour répondre aux attentes des membres._

Cette charte entraine une régulation des membres par les membres en lien avec les clauses de cette charte.



## Proccessus reproductibles pour une organisation forkable

L'adhésion à la charte peut être révoquée, révoquant ainsi la participation à la CAE.

La CAE a pour vocation de permettre la continuité de l'activité des membres dans les meilleures conditions, même suite au retrait de la CAE.

Pour cela, les processus propres à la CAE doivent être reproductibles (documentation et emploi de logiciels libres) et accessibles en ligne sous licence libre.

Dès lors, si un consensus ne peut être obtenu sur une évolution de la charte, un sous-groupe de la CAE peut quitter la CAE (avec les conséquences sur la dynamique du groupe – les règles en termes de parts sociales s'appliquant),

et **forker** la CAE en créant une autre CAE utilisant la même base d'organisation (statuts, organisation interne, etc) avec les modifications de la charte (ou de tout autre élément de la CAE) qui auront été souhaitées par le groupe.

Il est à noter qu'une fusion des deux CAE peut être envisagée s'il y a alignement des façons de fonctionner et des status et chartes des deux CAE.
